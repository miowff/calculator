﻿
namespace Task5Calculator
{
    class SystemMessages
    {
        public const string InputExpressionRequest = "Enter your expression";
        public const string WrongInputAlert = "Wrong input!Enter correct value again";
    }
}

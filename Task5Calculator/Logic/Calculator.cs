﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
namespace Task5Calculator
{
    public abstract class Calculator
    {
        private const string Operators = "+-/*";
        public abstract Regex regex { get; set; }
        public abstract void Calculate();
        private Result result;
        protected Result GetResult(string expression)
        {
            result = new Result();
            result.expression = expression;
            if (!IsStringInCorrectFormat(expression))
            {
                result.isExpressionValid = false;
                return result;
            }
            string parsedExpression = GetParsedString(expression);
            result.resultOfExpression = ParsedExpressionProcessor(parsedExpression);
            return result;
        }
        protected bool IsStringInCorrectFormat(string checkingString)
        {
            if (string.IsNullOrEmpty(checkingString))
            {
                throw new ArgumentNullException(nameof(checkingString));
            }
            if (regex.IsMatch(checkingString))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        protected virtual string GetParsedString(string _inputString)
        {
            if (string.IsNullOrEmpty(_inputString))
            {
                throw new ArgumentNullException(nameof(_inputString));
            }
            Stack<char> symbols = new Stack<char>();
            StringBuilder outputBuilder = new StringBuilder();
            for (int i = 0; i < _inputString.Length; i++)
            {
                if (char.IsDigit(_inputString[i]))
                {

                    outputBuilder.Append(GetNumber(_inputString, ref i));
                    continue;

                }
                outputBuilder.Append(SymbolProcessor(_inputString, ref i, symbols));
            }
            return outputBuilder.Append(new string(symbols.ToArray())).ToString();
        }
        protected string SymbolProcessor(string inputString, ref int i, Stack<char> symbols)
        {
            StringBuilder outputBuilder = new StringBuilder();
            if (symbols.Count == 0)
            {
                symbols.Push(char.Parse(inputString[i].ToString()));
                return outputBuilder.ToString();
            }
            else
            {
                if (IsOperator(inputString[i - 1]) & char.IsDigit(inputString[i + 1]) & GetPriority(inputString[i]) < GetPriority(inputString[i - 1]))
                {
                    outputBuilder.Append("0" + " " + inputString[i + 1] + " " + char.ConvertFromUtf32(inputString[i]));
                    if (i + 2 >= inputString.Length)
                    {
                        i++;
                        return outputBuilder.ToString();
                    }
                    else
                    {
                        i += 2;
                    }
                }
                if (GetPriority(inputString[i]) <= GetPriority(symbols.Peek()))
                {
                    while (symbols.Count > 0 && GetPriority(inputString[i]) <= GetPriority(symbols.Peek()))
                    {
                        outputBuilder.Append(symbols.Pop().ToString());
                    }
                    symbols.Push(char.Parse(inputString[i].ToString()));
                    return outputBuilder.ToString();
                }
                symbols.Push(char.Parse(inputString[i].ToString()));
            }
            return outputBuilder.ToString();
        }
        protected string GetNumber(string inputString, ref int i)
        {
            string output = string.Empty;
            while (char.IsDigit(inputString[i]) || inputString[i] == ',' || inputString[i] == '.')
            {
                output += inputString[i];
                i++;
                if (i == inputString.Length)
                {
                    break;
                }
            }
            i--;
            output += " ";
            return output;
        }
        private double ParsedExpressionProcessor(string parsedExpression)
        {
            Stack<double> numbers = new Stack<double>();
            for (int i = 0; i < parsedExpression.Length; i++)
            {
                if (char.IsDigit(parsedExpression[i]))
                {
                    numbers.Push(Convert.ToDouble(ReturnNumber(parsedExpression, ref i), CultureInfo.InvariantCulture.NumberFormat));
                    continue;
                }
                else
                {
                    if (numbers.Peek() == 0 & parsedExpression[i] == '/')
                    {
                        result.isExpressionValid = false;
                    }
                    if (numbers.Count == 1 && i != parsedExpression.Length)
                    {
                        double stackNumber = numbers.Pop();
                        numbers.Push(0);
                        numbers.Push(stackNumber);
                    }
                    numbers.Push(GetOperationResult(numbers.Pop(), numbers.Pop(), parsedExpression[i]));
                }
            }
            return Convert.ToDouble(numbers.Peek());
        }
        private string ReturnNumber(string parsedExpression, ref int i)
        {
            string number = string.Empty;
            int indexOfSpace = parsedExpression.IndexOf(' ', i);
            if (indexOfSpace != -1)
            {
                number = parsedExpression.Substring(i, indexOfSpace - i);
                i = indexOfSpace;
            }
            return number;
        }
        private double GetOperationResult(double num1, double num2, char operation)
        {
            switch (operation)
            {
                case '+':
                    return num2 + num1;
                case '-':
                    return num2 - num1;
                case '*':
                    return num2 * num1;
                case '/':
                    return num2 / num1;
                default: throw new NotImplementedException();
            }
        }
        private int GetPriority(char operation)
        {
            if (char.IsWhiteSpace(operation))
            {
                throw new ArgumentNullException(nameof(operation));
            }
            switch (operation)
            {
                case '+': return ConstValues.PlusAndMinusPriority;
                case '-': return ConstValues.PlusAndMinusPriority;
                case '*':
                case '/': return ConstValues.MultiplicationAndDivisionPriority;
                default: return 0;
            }
        }
        private bool IsOperator(char symbol)
        {
            return Operators.IndexOf(symbol) != ConstValues.NotFoundSymbolIndex;
        }
    }
}

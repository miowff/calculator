﻿using System;
using System.Text;
namespace Task5Calculator
{
    public sealed class Result
    {
        public string expression { private get; set; }
        public double resultOfExpression { private get; set; }
        public bool isExpressionValid = true;
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (!isExpressionValid)
            {
                stringBuilder.AppendLine(expression + "=" + "error");
            }
            else
            {
                stringBuilder.AppendLine(expression + "=" + resultOfExpression);
            }
            string result = stringBuilder.ToString();
            return result;
        }
    }
}

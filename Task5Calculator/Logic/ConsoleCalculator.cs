﻿using System;
using System.Text.RegularExpressions;
namespace Task5Calculator
{
    public sealed class ConsoleCalculator : Calculator
    {
        private readonly IConsoleCommuinication _consoleCommunication;
        public override Regex regex { get; set; }

        public ConsoleCalculator(IConsoleCommuinication consoleCommunication)
        {
            this.regex= new Regex(@"^[-+]{0,2}[0-9]+[.,)]{0,2}([,.+*/-][-+.,]{0,2}[0-9]+[,.)]{0,2})*$");
            this._consoleCommunication = consoleCommunication ?? throw new ArgumentNullException(nameof(consoleCommunication));
        }
        public override void Calculate()
        {
            _consoleCommunication.PrintMessage(SystemMessages.InputExpressionRequest);
            string expression = _consoleCommunication.GetInput();
            _consoleCommunication.PrintMessage(GetResult(expression).ToString());
        }
    }
}

﻿using System.IO;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;
namespace Task5Calculator
{
    public sealed class FileCalculator : Calculator
    {
        private readonly IFileManager _fileManager;
        public override Regex regex { get; set; }
        public string path;
        private string _initialFileName = @"\Result";
        private string _initialExtention = ".txt";
     
        public FileCalculator(IFileManager fileManager)
        {
            this.regex = new Regex(@"^[-+(]{0,2}[0-9]+[-+)]{0,2}([-+*/][-+(]*[0-9]+[-+)]{0,2})*$");
            this._fileManager = fileManager ?? throw new ArgumentNullException(nameof(fileManager));
            this.path = GetPath();
        }
        public override void Calculate()
        {
            string[] linesFromFile = _fileManager.GetInput(path);
            string[] result = new string[linesFromFile.Length];
            for (int i = 0; i < linesFromFile.Length; i++)
            {
                if (!IsOpenBracketEqualToCloseBracket(linesFromFile[i]))
                {
                    result[i] = linesFromFile[i] + "=error";
                }
                else
                {
                    result[i] = GetResult(linesFromFile[i]).ToString();
                }
            }
            string savePath = GetSavePath(path);
            _fileManager.WriteResult(savePath, result);
        }
        protected override string GetParsedString(string _inputString)
        {
            if (string.IsNullOrEmpty(_inputString))
            {
                throw new ArgumentNullException(nameof(_inputString));
            }
            Stack<char> symbols = new Stack<char>();
            StringBuilder outputBuilder = new StringBuilder();
            for (int i = 0; i < _inputString.Length; i++)
            {
                if (char.IsDigit(_inputString[i]))
                {
                    outputBuilder.Append(GetNumber(_inputString, ref i));
                    continue;
                }
                else
                {
                    if (_inputString[i] == '(')
                    {
                        
                        symbols.Push((_inputString[i]));
                    }
                    else if (_inputString[i] == ')')
                    {
                        outputBuilder.Append(BracketsProcessor(symbols));
                    }
                    else
                    {
                        outputBuilder.Append(SymbolProcessor(_inputString, ref i, symbols));
                    }
                }
            }
            outputBuilder.Append(new string(symbols.ToArray()));
            return outputBuilder.ToString();
        }
        private string BracketsProcessor(Stack<char> symbols)
        {
            char symbol = symbols.Pop();
            StringBuilder outputBuilder = new StringBuilder();
            while (symbol != '(')
            {
                outputBuilder.Append(symbol.ToString());
                symbol = symbols.Pop();
            }
            return outputBuilder.ToString();
        }
        private string GetPath()
        {
            Regex regex = new Regex("((?:[^/]*/)*)");
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            foreach (var argument in commandLineArgs)
            {
                if (regex.IsMatch(argument))
                {
                    path = argument;
                }
            }
            return path;
        }
        private string GetSavePath(string _path)
        {
            string newPath = Path.GetDirectoryName(_path);
            return newPath + _initialFileName+_initialExtention;
        }
        private bool IsOpenBracketEqualToCloseBracket(string inputString)
        {
            int openBracketCount = 0;
            int closeBracketCount = 0;
            for (int i = 0; i < inputString.Length; i++)
            {
                if (inputString[i] == '(')
                {
                    openBracketCount++;
                    continue;
                }
                if (inputString[i] == ')')
                {
                    closeBracketCount++;
                }
            }
            return openBracketCount == closeBracketCount;
        }
    }
}

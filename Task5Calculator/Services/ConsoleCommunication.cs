﻿using System;
namespace Task5Calculator
{
    public sealed class ConsoleCommunication : IConsoleCommuinication
    {
        public string GetInput()
        {
            string message = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(message))
            {
                Console.WriteLine(SystemMessages.WrongInputAlert);
                message = Console.ReadLine();
            }
            return message;

        }
        public void PrintMessage(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }
            Console.WriteLine(message);
        }
    }
}

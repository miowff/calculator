﻿using System.IO;
using System;
namespace Task5Calculator
{
    public sealed class FileManager : IFileManager
    {
        public string[] GetInput(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (File.ReadAllLines(path).Length==0)
            {
                throw new ArgumentNullException(nameof(path));
            }
            string[] lines = File.ReadAllLines(path);
            return lines;
        }
        public void WriteResult(string path, params string[] results)
        {
            if (string.IsNullOrEmpty(path))
            {

                throw new ArgumentNullException(nameof(path));
            }

            if (results == null)
            {
                throw new ArgumentNullException(nameof(results));
            }

            using (StreamWriter writer = new StreamWriter(path, false, System.Text.Encoding.Default))
            {
                foreach (var expression in results)
                {
                    writer.Write(expression);
                }
                writer.Close();
            }
        }

    }
}

﻿using System;
using System.Text.RegularExpressions;
using Microsoft.Extensions.DependencyInjection;
namespace Task5Calculator
{
    public sealed class ContainerBuilder
    {
        public IServiceProvider Build()
        {
            var container = new ServiceCollection();
            Regex regex = new Regex("((?:[^/]*/)*)(.)");
            string[] commandLineArgs = Environment.GetCommandLineArgs();
            if (commandLineArgs.Length > 1)
            {
                foreach (var argument in commandLineArgs)
                {
                    if (regex.IsMatch(argument))
                    {
                        container.AddSingleton<Calculator, FileCalculator>();
                        container.AddSingleton<IFileManager, FileManager>();
                    }
                }
            }
            else
            {
                container.AddSingleton<Calculator, ConsoleCalculator>();
                container.AddSingleton<IConsoleCommuinication, ConsoleCommunication>();
               
            }
            return container.BuildServiceProvider();
        }
    }
}

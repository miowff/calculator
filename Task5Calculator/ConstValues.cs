﻿namespace Task5Calculator
{
    public class ConstValues
    {
        public const int PlusAndMinusPriority = 2;
        public const int MultiplicationAndDivisionPriority = 3;
        public const int NotFoundSymbolIndex = -1;
    }
}

﻿namespace Task5Calculator
{
    public interface IFileManager
    {
        public string[] GetInput(string path);
        public void WriteResult(string path, params string[] results);
    }
}

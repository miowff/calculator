﻿namespace Task5Calculator
{
    public interface IConsoleCommuinication
    {
        public string GetInput();
        public void PrintMessage(string message);
    }
}

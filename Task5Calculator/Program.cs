﻿using System;
using Microsoft.Extensions.DependencyInjection;
namespace Task5Calculator
{
    class Program
    {
        public static readonly IServiceProvider Container = new ContainerBuilder().Build();
        static void Main(string[] args)
        {
            var calculator = Container.GetService<Calculator>();
            calculator.Calculate();
        }

    }
}


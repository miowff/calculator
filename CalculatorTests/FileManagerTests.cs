﻿using NUnit.Framework;
using Task5Calculator;
using System.IO;
using System;
namespace CalculatorTests
{
    class FileManagerTests
    {
        private FileManager _fileManager;
        private string path;
        [SetUp]
        public void Setup()
        {
            _fileManager = new FileManager();
             path = Path.Combine(Environment.CurrentDirectory, "FileManagerTest.txt");
        }

        [Test]
        public void GetInputFromFileTest()
        {
            //arrange
            string[] expressions = { "2+2*2", "2+x*2", "15+5*2" };
            var myFile = File.Create(path);
            myFile.Close();
            //act
            File.AppendAllLines(path,expressions);
            //assert
            Assert.AreEqual(expressions.Length,_fileManager.GetInput(path).Length);
            File.Delete(path);
        }

        [Test]
        public void NullInput()
        {
            // Arrange
            string expected = null;
            //Assert
            Assert.Throws<ArgumentNullException>(() => _fileManager.GetInput(expected));
        }

        [Test]
        public void ReadFromEmptyFileTest()
        {
            //arrange
            var myFile = File.Create(path);
            myFile.Close();
            //act
            Assert.Throws<ArgumentNullException>(() => _fileManager.GetInput(path));
        }
        [Test]
        public void WriteResultTest()
        {
            //Arrange
            string[] expressions = { "2+2*2=6"};
            var myFile = File.Create(path);
            myFile.Close();
            //act
            _fileManager.WriteResult(path,expressions);
            //Assert
            Assert.AreEqual(expressions.Length,File.ReadAllLines(path).Length);
            File.Delete(path);
        }
    }
}

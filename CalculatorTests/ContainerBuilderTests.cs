﻿using NUnit.Framework;
using Task5Calculator;
using System;
using Microsoft.Extensions.DependencyInjection;
namespace CalculatorTests
{
    class ContainerBuilderTests
    {
        public static readonly IServiceProvider Container = new ContainerBuilder().Build();
        [Test]
        public void ContainerBuilderIsNotNull()
        {
            //Assert
            Assert.NotNull(Container);
        }
        [Test]
        public void IsProvidedContainsConsoleCommunication()
        {
            //Arrange
            var consoleCommunication = Container.GetService<IConsoleCommuinication>();
            //Assert
            Assert.NotNull(consoleCommunication);
        }
        [Test]
        public void IsProvidedContainsIFileReader()
        {
            //Arrange
            var fileManager= Container.GetService<IFileManager>();
            //Assert
            Assert.NotNull(fileManager);
        }
        [Test]
        public void IsProvidedCalculator()
        {
            //Arrange
            var Calculator = Container.GetService<Calculator>();
            //Assert
            Assert.NotNull(Calculator);
        }

    }
}

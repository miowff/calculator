﻿using NUnit.Framework;
using Task5Calculator;
using Moq;
namespace CalculatorTests
{
    public class ConsoleCalculatorTests
    {
        private ConsoleCalculator _consoleCalculator;
        private readonly Mock<IConsoleCommuinication> _consoleCommunicationMoq = new Mock<IConsoleCommuinication>();
        [SetUp]
        public void Setup()
        {
            _consoleCalculator = new ConsoleCalculator(_consoleCommunicationMoq.Object);
        }

        [TestCase("0","0")]
        [TestCase("-3", "-3")]
        [TestCase("3*4", "12")]
        [TestCase("1+2+3*4+5+6", "26")]
        [TestCase("1+2-3*4+5+6", "2")]
        [TestCase("1+2+3*-4+5+6", "2")]
        [TestCase("1+2-3*-4+5+6", "26")]
        [TestCase("+8/2", "4")]
        [TestCase("8/+2", "4")]
        [TestCase("-8/2", "-4")]
        [TestCase("8/-2", "-4")]
        [TestCase("-8/-2", "4")]
        [TestCase("9/2", "4,5")]
        [TestCase("+16/2", "8")]
        [TestCase("2*11", "22")]
        [TestCase("12--12", "-24")]
        [TestCase("-12--12", "0")]
        [TestCase("2+3+4+2+1", "12")]
        [TestCase("2+3-4+2-1", "2")]
        [TestCase("2+3-4*2-1", "-4")]
        [TestCase("2+3-4/2-1", "2")]
        [TestCase("2+3-4/-2*3-1+7*3", "31")]
        [TestCase("2+3+4/-2/2-1", "3")]
        [TestCase("2+15/3+4*2", "15")]
        [TestCase("2+3*5/2+1*4", "13,5")]
        [TestCase("2+24/+8*+2+3/2", "9,5")]
        [TestCase("-2+36/-9*+3+3/-1", "-17")]
        [TestCase("2-36/9*-3+1*5", "19")]
        [TestCase("2,5+2,5", "5")]
        [TestCase("2.5+2,5", "5")]
        [TestCase("10,5+5", "15,5")]
        [TestCase("10.5+5", "15,5")]
        [TestCase("2............5", "error")]
        [TestCase("2,,,,,,,,,,,,,,,5", "error")]
        [TestCase("null", "error")]
        [TestCase("(1+1)+1", "error")]
        [TestCase("7****8", "error")]
        [TestCase("4-+*8", "error")]
        [TestCase("15-5/*8", "error")]
        [TestCase("1*2+x-8", "error")]
        [TestCase("10O0", "error")]
        [TestCase("5-5=-0", "error")]
        public void CalculateCorrectExpression(string input, string expected)
        {
            //Arrange
            _consoleCommunicationMoq.Setup(x => x.PrintMessage(It.IsAny<string>()));
            _consoleCommunicationMoq.Setup(x => x.GetInput()).Returns(input);
            //Act
            _consoleCalculator.Calculate();
            //Assert
            _consoleCommunicationMoq.Verify(x => x.PrintMessage($"{input + "=" + expected}\r\n"), Times.Once);
        }

    }
}

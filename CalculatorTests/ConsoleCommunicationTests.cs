using NUnit.Framework;
using Task5Calculator;
using System.IO;
using System;
namespace CalculatorTests
{
    public class ConsoleCommuinicationTests
    {
        private ConsoleCommunication _consoleCommunication;
        [SetUp]
        public void Setup()
        {
            _consoleCommunication = new ConsoleCommunication();
        }

        [Test]
        public void GetInputTest()
        {
            //Arrange
            string expected = "2+2*2";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            var actual = _consoleCommunication.GetInput();
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void PrintMessageTest()
        {
            //Arrange
            var output = new StringWriter();
            Console.SetOut(output);
            //Act
            _consoleCommunication.PrintMessage("message");
            //Assert
            Assert.That(output.ToString(), Is.EqualTo(string.Format("message\r\n", Environment.NewLine)));
        }

        [Test]
        public void NullMessageTest()
        {
            // Arrange
            string expected = null;
            //Assert
            Assert.Throws<ArgumentNullException>(() => _consoleCommunication.PrintMessage(expected));
        }
    }
}
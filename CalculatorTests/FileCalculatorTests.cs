﻿using NUnit.Framework;
using Task5Calculator;
using System.Linq;
using Moq;
namespace CalculatorTests
{
    class FileCalculatorTests
    {
        private FileCalculator _fileCalculator;
        private readonly Mock<IFileManager> _fileManagerMoq = new Mock<IFileManager>();
        [SetUp]
        public void Setup()
        {
            _fileCalculator = new FileCalculator(_fileManagerMoq.Object);
        }
        
        [TestCase("(1)","(1)=1\r\n")]
        [TestCase("1+(1)+1", "1+(1)+1=3\r\n")]
        [TestCase("(-5)", "(-5)=-5\r\n")]
        [TestCase("-(5)", "-(5)=-5\r\n")]
        [TestCase("33+(2-5)", "33+(2-5)=30\r\n")]
        [TestCase("12+(-12)", "12+(-12)=0\r\n")]
        [TestCase("1+2*(3+2)", "1+2*(3+2)=11\r\n")]
        [TestCase("(2+3)-(4+3)", "(2+3)-(4+3)=-2\r\n")]
        [TestCase("(2,5+2.5)-(2.5+2,5)", "(2,5+2.5)-(2.5+2,5)=0\r\n")]
        [TestCase("(10,5+10.5)-(10.5+10,5)", "(10,5+10.5)-(10.5+10,5)=0\r\n")]
        [TestCase("1+2-(3+(4+5)-6)-2", "1+2-(3+(4+5)-6)-2=-5\r\n")]
        [TestCase("12+((((12)+8)+7)+6)", "12+((((12)+8)+7)+6)=45\r\n")]
        [TestCase("12+(3-(8+5))+1", "12+(3-(8+5))+1=3\r\n")]
        [TestCase("-(10,5)", "-(10,5)=-10,5\r\n")]
        [TestCase("10.15+10,15", "10,15+10.15=20,3\r\n")]
        [TestCase("(.....)", "(.....)=error\r\n")]
        [TestCase("(,,,,,,,,)", "(,,,,,,,,)=error\r\n")]
        [TestCase("null", "null=error\r\n")]
        [TestCase("2/(2,5-2.5)", "2/(2,5-2.5)=error\r\n")]
        [TestCase("(1+1)+1=3", "(1+1)+1=3=error\r\n")]
        [TestCase("(a+3)-4", "(a+3)-4=error\r\n")]
        [TestCase("20O0", "20O0=error\r\n")]
        [TestCase("6-6=-0", "6-6=-0=error\r\n")]
        [TestCase("6-6/(2-2)", "6-6/(2-2)=error\r\n")]
        [TestCase("6-6/(2-(45+(45)-))", "6-6/(2-(45+(45)-))=error\r\n")]
        public void CalculateFileExpressions(string expression,string expected)
        {
          //Arrange
          string actual = string.Empty;
          _fileManagerMoq.Setup(x => x.GetInput(It.IsAny<string>())).Returns(new[] {expression});
          _fileManagerMoq.Setup(x => x.WriteResult(It.IsAny<string>(), It.IsAny<string[]>()))
              .Callback((string path, string[] arr) =>
              {
                  actual = arr.FirstOrDefault();
              });
        }

    }
}

